import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import{AngularFireDatabase,FirebaseListObservable} from 'angularfire2/database';

import { ProductosPage } from '../productos/productos';
import {productositem} from '../../models/registrar-item/productos-item.interface'

/**
 * Generated class for the ShoppinnListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
 	selector: 'page-shoppinn-list',
 	templateUrl: 'shoppinn-list.html',
 })
 export class ShoppinnListPage {

 	items;
 	shoppinnListRef$:any;

 	constructor(public navCtrl: NavController, public navParams: NavParams,private database:AngularFireDatabase) {

 		this.database.list('productos-list').valueChanges().subscribe((resp)=>{
 			this.shoppinnListRef$ = resp;
 			this.initializeItems();
 		});

 	}



initializeItems() {
 this.items=this.shoppinnListRef$;
  }


 getItems(ev: any) {

 	this.initializeItems();

    const val = ev.target.value;

    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.itemproducto.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }

 }
 }
