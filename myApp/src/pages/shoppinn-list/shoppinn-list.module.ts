import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoppinnListPage } from './shoppinn-list';

@NgModule({
  declarations: [
    ShoppinnListPage,
  ],
  imports: [
    IonicPageModule.forChild(ShoppinnListPage),
  ],
})
export class ShoppinnListPageModule {}
