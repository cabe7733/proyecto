import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase,FirebaseListObservable} from 'angularfire2/database';
import { categoriaitem } from '../../models/registrar-item/categoria-item.interface';


@IonicPage()
@Component({
  selector: 'page-categoria',
  templateUrl: 'categoria.html',
})
export class CategoriaPage {

	categoriaitem ={} as categoriaitem
 	categoriaitemRef$:FirebaseListObservable<categoriaitem[]>


  constructor(public navCtrl: NavController, public navParams: NavParams, private database:AngularFireDatabase) {
  this.categoriaitemRef$=this.database.list('categoria-list');
  }

  agregac(categoriaitem:categoriaitem){

  		this.categoriaitemRef$.push({
			itemnomcategoria:this.categoriaitem.itemnomcategoria
	    });

	this.categoriaitem ={} as categoriaitem;
  this.navCtrl.pop();	

  } 


}
