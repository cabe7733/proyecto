import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase,FirebaseListObservable} from 'angularfire2/database';
import{registraitem} from '../../models/registrar-item/registrar-item.interface';

/**
 * Generated class for the RegistroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {
 
 registraitem={} as registraitem
registraitemRef$:FirebaseListObservable<registraitem[]>

  constructor(public navCtrl: NavController, public navParams: NavParams,private database:AngularFireDatabase) {
  	this.registraitemRef$=this.database.list('shopping-list');
  }

addregistro(registraitem: registraitem){
	this.registraitemRef$.push({

		itemnombre:this.registraitem.itemnombre,
		itemapellido:this.registraitem.itemapellido,
		itemcorreo:this.registraitem.itemcorreo,
		itempass:this.registraitem.itempass

	});

	this.registraitem={} as registraitem;

	this.navCtrl.pop();
}

}
