import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';

import {productositem} from '../../models/registrar-item/productos-item.interface';
import { HomePage } from '../home/home';

/**
 * Generated class for the ProductosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-productos',
  templateUrl: 'productos.html',
})
export class ProductosPage {

 productositem ={} as productositem
 productositemRef$:FirebaseListObservable<productositem[]>

  constructor(public navCtrl: NavController,public navParams: NavParams,private database:AngularFireDatabase) {
  this.productositemRef$=this.database.list('productos-list');
  }

    

  agrega(productositem:productositem){

  		this.productositemRef$.push({

		itemproducto:this.productositem.itemproducto,
		itemdescripcion:this.productositem.itemdescripcion,
		itemimagen:this.productositem.itemimagen,
		itemcategoria:this.productositem.itemcategoria

	});

	this.productositem={} as productositem;
  this.navCtrl.pop();	

  } 



  }

