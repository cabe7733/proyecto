import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductosPage } from '../productos/productos';
import { ShoppinnListPage } from '../shoppinn-list/shoppinn-list';
import { CategoriaPage } from '../categoria/categoria';
import { LoadingController } from 'ionic-angular';


/**
 * Generated class for the ProductoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-producto',
  templateUrl: 'producto.html',
})
export class ProductoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController) {
  }


  	otraPagina(){
		this.navCtrl.push(ProductosPage);
		this.loadingCtrl.create({
      content: 'Espere un momento...',
      duration: 9000,
      dismissOnPageChange: true
    }).present();

	}

	nuevaPagina(){
		  this.navCtrl.push(ShoppinnListPage);
		  
		  this.loadingCtrl.create({
	      content: 'Espere un momento...',
	      duration: 9000,
	      dismissOnPageChange: true
	    }).present();
	}


 categorias(){
		  this.navCtrl.push(CategoriaPage);

		  this.loadingCtrl.create({
	      content: 'Espere un momento...',
	      duration: 9000,
	      dismissOnPageChange: true
	    }).present();
	}


  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductoPage');
  }

}
