import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{ AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { ShoppinnListPage } from '../shoppinn-list/shoppinn-list';
import { categoriaitem } from '../../models/registrar-item/categoria-item.interface'
import { CategoriaPage } from '../categoria/categoria';
import { LoadingController } from 'ionic-angular';



@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  items;
  categoriaListRef$:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,private database:AngularFireDatabase) {

              this.database.list('categoria-list').valueChanges().subscribe((resp)=>{
                this.categoriaListRef$ = resp;
                this.initializeItems();
              });
  }


  initializeItems() {
      this.items=this.categoriaListRef$;
    }


 getItems(ev: any) {

    this.initializeItems();

    const val = ev.target.value;

    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.itemnomcategoria.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }

 }


 nuevaPagina(){
      this.navCtrl.push(ShoppinnListPage);
      
      this.loadingCtrl.create({
        content: 'Espere un momento...',
        duration: 9000,
        dismissOnPageChange: true
      }).present();
  }

 

}
