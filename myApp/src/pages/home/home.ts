import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RegistroPage } from '../registro/registro';
import { SesionPage } from '../sesion/sesion';
import { LoadingController } from 'ionic-angular';


@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})


export class HomePage {

	constructor(public navCtrl: NavController,public loadingCtrl: LoadingController) {

	}

	otraPagina(){
		this.navCtrl.push(RegistroPage);
		this.loadingCtrl.create({
      content: 'Espere un momento...',
      duration: 9000,
      dismissOnPageChange: true
    }).present();
	}

	nuevaPagina(){
		this.navCtrl.push(SesionPage);
		this.loadingCtrl.create({
      content: 'Espere un momento...',
      duration: 9000,
      dismissOnPageChange: true
    }).present();
	}

}


export class BackgroundPage { }
