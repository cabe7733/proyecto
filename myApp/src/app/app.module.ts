import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { RegistroPage } from '../pages/registro/registro';
import { ProductosPage } from '../pages/productos/productos';
import { ProductoPage } from '../pages/producto/producto';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FIREBASE_CREDENTIALS } from './firebase.credentials';
import { SesionPage } from '../pages/sesion/sesion';
import {ShoppinnListPage} from '../pages/shoppinn-list/shoppinn-list';
import { CategoriaPage } from '../pages/categoria/categoria';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
  MyApp,
  AboutPage,
  HomePage,
  TabsPage,
  RegistroPage,
  ProductosPage,
  ProductoPage,
  SesionPage,
  ShoppinnListPage,
  CategoriaPage 

  ],
  imports: [
  BrowserModule,
  IonicModule.forRoot(MyApp)
  
  AngularFireModule.initializeApp(FIREBASE_CREDENTIALS),
  AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
  MyApp,
  AboutPage, 
  HomePage,
  TabsPage,
  RegistroPage,
  ProductosPage,
  ProductoPage,
  SesionPage,
  ShoppinnListPage,
  CategoriaPage

  ],
  providers: [
  StatusBar,
  SplashScreen,
  {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
